import React from "react";
import logo from "./logo.svg";
import "./App.css";
import Attendance from "./Attendance";

function App() {
  return (
    <div className="App">
      <h2>Hello</h2>
      <Attendance title="Attendance codes" />
    </div>
  );
}

export default App;
